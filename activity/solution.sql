-- Add the following records to the blogpots_db database:
    -- Users
        -- (Email, Password, DatetimeCreated, username  (
        --     {"johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00", "johnsmith0101"},
        --     {juandelacruz@gmail.com, passwordB, 2021-01-02 00:00, 1delacruz01},
        --     {janesmith@gmail.com, passwordC, 2021-01-01 03:00:00, janesmithunique},
        --     {mariadelacruz@gmail.com, passwordD, 2021-01-01 04:00:00, mariaDC},
        --     {johndoe@gmail.com, passwordE, 2021-01-01 05:00:00, anonJ},
        -- )
-- INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 2);
INSERT INTO users (email, password, datetime_created, username) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00", "johnsmith0101");
INSERT INTO users (email, password, datetime_created, username) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00", "1delacruz01");
INSERT INTO users (email, password, datetime_created, username) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00", "janesmithunique");
INSERT INTO users (email, password, datetime_created, username) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00", "mariaDC");
INSERT INTO users (email, password, datetime_created, username) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00", "anonJ");

-- Add the following records to the blog_db database:
    -- Posts
    -- INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Ice Cream', '00:04:16', 'Kpop', 1);
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all the posts with an Author ID of 1.
-- SELECT song_name, genre FROM songs;
-- SELECT song_name FROM songs WHERE genre = "OPM";
SELECT title, content FROM posts WHERE author_id = 1;

-- Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!" Where its initial content is "Hello Earth!" by using the record's ID.
-- UPDATE songs SET length = 400 WHERE song_name = "You Never Know";
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!" AND id = 2;

-- Delete the user with an email of "johndoe@gmail.com".
-- DELETE FROM songs WHERE genre = "Kpop" AND length > 400;
DELETE FROM users WHERE email = "johndoe@gmail.com";
