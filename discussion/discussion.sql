-- [SECTION] CRUD Operations

-- Inserting Records (Create)

-- To insert an artist in the artists table:
INSERT INTO artists(name) VALUES ("Nickleback");
INSERT INTO artists(name) VALUES ("BlackPink");

-- To insert albums in the albums table:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1995-01-01", 2);

-- To insert songs in the songs table:
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ice Cream", 256, "K-Pop", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("You Never Know", 239, "K-Pop", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Rockstar", 234, "OPM", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Photograph", 279, "OPM", 1);